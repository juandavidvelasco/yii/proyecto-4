<?php
use yii\helpers\Html;
use yii\widgets\ListView;
use yii\web\View;
use yii\debug\models\timeline\DataProvider;
?>

<?php 
    $this->registerJs("$('#recipeCarousel').carousel({
        interval: 9000000
        })"
        ,View::POS_READY);
        
    $this->registerJs(" $('.carousel .carousel-item').each(function(){
            var minPerSlide = 2;
            var next = $(this).next();
            if (!next.length) {
            next = $(this).siblings(':first');
            }
            next.children(':first-child').clone().appendTo($(this));

            for (var i=0;i<minPerSlide;i++) {
                next=next.next();
                if (!next.length) {
                        next = $(this).siblings(':first');
                }

                next.children(':first-child').clone().appendTo($(this));
              }
        });",
          View::POS_READY);
    
    $this->registerCss("@media (max-width: 768px) {
            .carousel-inner .carousel-item > div {
                display: none;
            }
            .carousel-inner .carousel-item > div:first-child {
                display: block;
            }
        }

        .carousel-inner .carousel-item.active,
        .carousel-inner .carousel-item-next,
        .carousel-inner .carousel-item-prev {
            display: flex;
        }

        /* display 3 */
        @media (min-width: 768px) {

            .carousel-inner .carousel-item-right.active,
            .carousel-inner .carousel-item-next {
              transform: translateX(33.333%);
            }

            .carousel-inner .carousel-item-left.active, 
            .carousel-inner .carousel-item-prev {
              transform: translateX(-33.333%);
            }
        }

        .carousel-inner .carousel-item-right,
        .carousel-inner .carousel-item-left{ 
          transform: translateX(0);
        }"
            );
    ?>


<div>
    

        <div class="flex-container">
            <div style="flex-grow: 5; flex-shrink: 250; align-items: center">
                <?php echo Html::img('@web/images/ciclistas/'.$ciclista[0]['dorsal'].'.jpg',
                    ['width'=>'100%','height'=>'100%',
                    'type'=>"button", 'data-toggle'=>"modal", 'data-target'=>"#imagen", 'data-whatever'=>"@getbootstrap"]) 
                ?> 
            </div>
            <div style="flex-grow: 5">

                <p> <?=  ($ciclista[0]['nombre']) ?> </p>
                <p> Edad: <?=  ($ciclista[0]['edad']) ?> </p>
                <p> Nombre del equipo: <?=  ($ciclista[0]['nomequipo']) ?> </p>
                <p>Total de etapas Ganadas: <?= implode($etapasGandas[0]) ?>  </p>

                <?php if(!empty($maillotUtilizados)) {

                   for ($i = 0; $i <= count($maillotUtilizados)-1; $i++) {


                ?>
                <P style="margin-top: 15px"> Tipo: <?= ($maillotUtilizados[$i]['tipo']) ?> </P>
                <p> Color: <?=  ($maillotUtilizados[$i]['color'])  ?> </p>
                <p> Código del maillot <?=  ($maillotUtilizados[$i]['código']) ?>  </p>
                <p> Etapa en la que le utilizo <?=  ($maillotUtilizados[$i]['numetapa']) ?> </p>
                <p style="margin-bottom: 15px"> Premio ganado <?=  ($maillotUtilizados[$i]['premio']) ?> </p>

                <?php 

                    }
                    }

                ?>

            </div>
        </div>


     <div class="row mx-auto my-auto">
        <div id="recipeCarousel" class="carousel slide w-100" data-ride="carousel">
            <div class="carousel-inner w-100" role="listbox">
                <div class="carousel-item active">
                    <div class="col-md-4">
                        <div class="card card-body">
                           <?= Html::img('@web/images/equipos/equipos.jpg',['width'=>'100%','height'=>'300px']) ?>
                            <p id="nomequipo">
                                Selecciona un equipo
                            </p>
                        </div>
                    </div>
                </div>
                
    <!--OBJETOS PARA LOS EQUIPOS-->
        
                    <?= ListView::widget([
                        'dataProvider' => $dataProvider,
                        #'itemView' => '_jugadores',
                        #'options'=>['class'=>'col-sm-6 col-md-4'],
                        #'itemView'=>['class'=>'card tarjeta']
                        'layout'=>"{items}",
                        'itemView'=> function($model){

                    ?>
    
                <div class="carousel-item">
                    <div class="col-md-4">
                        <div class="card card-body">
                            <?= Html::a(Html::img('@web/images/ciclistas/'.$model['dorsal'].'.jpg',['width'=>'100%','height'=>'300px']),
                                    ['site/jugadores','nombrequipo'=>$model['nomequipo']],
                                    ['class'=>'img-fluid']) ?>
                            <p id="nomequipo">
                                <?= $model['nomequipo'] ?>
                            </p>
                        </div>
                    </div>
                </div>
                   
                    <?php 
                        }
                        ]);
                    ?>
    
                    
            </div>
            <a class="carousel-control-prev w-auto" href="#recipeCarousel" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon bg-dark border border-dark rounded-circle" aria-hidden="true"></span>
                <span class="sr-only">anterior</span>
            </a>
            <a class="carousel-control-next w-auto" href="#recipeCarousel" role="button" data-slide="next">
                <span class="carousel-control-next-icon bg-dark border border-dark rounded-circle" aria-hidden="true"></span>
                <span class="sr-only">siguiente</span>
            </a>
            
        </div>
    </div> 


    <!--IMAGEN EN GRANDE--> 

    <div class="modal fade" id="imagen" tabindex="-1" role="dialog" aria-hidden="true" >
        <div class="modal-dialog" role="document">
            <div class="modal-content" >
                <div class="modal-body" style="padding:0" >

                    <?= Html::img("@web/images/ciclistas/".$ciclista[0]['dorsal'].".jpg",['width'=>'100%','height'=>'100%']) ?>

                </div>
            </div>
        </div> 
    </div>

    <!--fin de la imagen en grande-->

 
</div>  <!--Cierra el div principal-->


