<?php

    use yii\helpers\Html;
    use yii\widgets\ActiveForm;
    use yii\widgets\ListView;
?>


<div class="site-index">
    <div class="row">
    <?= ListView::widget([
        'dataProvider' => $dataProvider,
        #'itemView' => '_jugadores',
        #'options'=>['class'=>'col-sm-6 col-md-4'],
        #'itemView'=>['class'=>'card tarjeta']
        'layout'=>"{items}",
        'viewParams'=> [
            
        ],
        'itemView'=> function($model){

    ?>
    
        <div class="col-sm-6 col-md-4">
                <div class="card alturaminimaJugadores">
                    <div class="card-body tarjeta">
                        <p #id="juga">
                            <?php echo Html::img('@web/images/ciclistas/'.$model->dorsal.'.jpg',['width'=>'100%','height'=>'300px','class'=>'juga']); ?>
                        </p>
                        <p>
                            Nombre: <?= $model->nombre?>
                        </p>
                        <p>
                            Dorsal: <?= $model->dorsal ?>
                        </p>
                        <?= Html::a('Ver ficha del jugador',['site/ficha','dorsal'=>$model->dorsal,'nombrequipo'=>$model['nomequipo']],['class'=>'btn btn-primary']) ?>
                    </div>
                </div>
           
        </div>
        
        <?php 
            }
            ]);
        ?>
   </div>              
</div>
